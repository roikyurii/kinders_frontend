import React, { Component } from 'react';
import './sass/App.sass';
import introImg from './img/main.png'
import KinderTable from './components/KinderTable'

class App extends Component {
  render (){
    return (
      <div className="App">
        <main className="index">
          <section className="progress">
            <div className="container">
              <div className="progress__inner">
                <img className="progress__title" src={introImg} alt="" />
                <KinderTable />
              </div>
            </div>
          </section>
        </main>
      </div>
    )
  }
}

export default App;
