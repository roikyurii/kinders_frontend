import React from 'react'
import KinderInTable from './KinderInTable'

const TableRow = (props) => {
  let {kinder, kinders_in_peoples, activeId, changeAmount} = props
  return(
    <div className="info-row" key={kinder.id}>
      <div className="info-cell">
        <img className="info-cell__toy" src={"/img/kinders/" + kinder.img_src} alt="" />
      </div>
      {kinders_in_peoples.map(own_kinder => {
        return (
          <KinderInTable 
            own_kinder={own_kinder} 
            activeId={activeId}
            curKinderID = {kinder.id}
            key = {own_kinder.id} 
            changeAmount = {changeAmount}
            />
        )
      })}
    </div>
  )
}

export default TableRow