import React from 'react'
import Icons from './Icons'
import AmountBtn from './AmountBtn'


const KinderInTable = (props) => {
  let {kinder_id, amount, id} = props.own_kinder
  let {curKinderID, activeId} = props
  let icon
  if (kinder_id === curKinderID) {
    switch (true) {
      case amount > 1:
        icon = Icons.inStockPlus
        break
      case amount === 1:
        icon = Icons.inStock
        break
      default:
        icon = Icons.missing
        break
    }
    return (
      <div className="info-cell">
        <span className={"info-cell__id " + (activeId ? 'active': '')}>{id}</span>
        {icon}
        <p className="info-cell__amount">Кількість: <span>{amount}</span></p>
        <div className={"info-cell__btns " + (activeId ? 'active': '')}>
          <AmountBtn action="-" id={id} changeAmount={props.changeAmount} />
          <AmountBtn action="+" id={id} changeAmount={props.changeAmount} />
        </div>
      </div>)
  } else {
    return (
      null
    )
  }
}

export default KinderInTable