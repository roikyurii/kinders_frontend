import React, { Component } from 'react';
import '../sass/components/KinderTable.sass'
import Preloader from './Preloader'
import logo from '../img/logo.png'
import PeopleInTable from './PeopleInTable'
import TableRow from './TableRow'
import Api from '../ApiConfig'



class KinderTable extends Component {
  constructor(props){
    super(props);
    this.state = {
      isLoaded: false,
      kinders: [],
      peoples: [],
      kinders_in_peoples: [],
    };
  }

  componentDidMount(){
    fetch(Api.url+'get_all')
      .then(res => res.json())
      .then(json => {
        this.setState({
          isLoaded: true,
          kinders: json.kinders,
          peoples: json.peoples,
          kinders_in_peoples: json.kinders_in_peoples,
          activeId: false
        })
      })
  }

  showNav = () => {
    this.setState(prevState => (
      { 
        activeId: !prevState.activeId 
      }
    )); 
  }

  changeAmount = (e) => {
    let action = e.target.getAttribute('data-action')
    let kinderId = +e.target.getAttribute('data-id')
    let sendAccess = false
    // Render new data
    this.setState(prevState => {
      let kinders_in_peoples_upd = prevState.kinders_in_peoples
      kinders_in_peoples_upd.map(k => {
        if (k.id===kinderId) {
          if (action === '+'){
            k.amount = k.amount + 1
            sendAccess = true
          } else {
            if (k.amount > 0) {
              k.amount = k.amount - 1
              sendAccess = true
            }
          }
          // Send new data
          if (sendAccess) {
            fetch(Api.url+'update_kinder_in_people/'+kinderId, {
              method: 'PUT',
              headers: {'Content-Type': 'application/json'},
              body: JSON.stringify({"amount": k.amount})
            })
            // .then(res => res.text()) // OR res.json()
            // .then(res => console.log(res))
          }
        }
        return null
      })
      return({
        kinders_in_peoples: kinders_in_peoples_upd
      })
    })
  }

  render() {
    var {isLoaded, kinders, kinders_in_peoples, peoples} = this.state;
    if (!isLoaded) {
      return (
        <Preloader />
      )
    } else {
      return (
        <div className="info">
          <div className="info-show" onClick={this.showNav}>Edit</div>
          <div className="info-head">
            <div className="info-cell">
              <img className="info-cell__logo" src={logo} alt="" />
            </div>
            {peoples.map(people => (
              <PeopleInTable key={people.id} people={people} />
            ))}
          </div>
          {kinders.map(kinder => (
            <TableRow
              key = {kinder.id} 
              kinder={kinder} 
              kinders_in_peoples={kinders_in_peoples} 
              activeId={this.state.activeId} 
              changeAmount = {this.changeAmount}
            />
          ))}
        </div>
      );
    }
  }
}

export default KinderTable;
