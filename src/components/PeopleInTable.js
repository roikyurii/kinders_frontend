import React from 'react'

const PeopleInTable = (props) => {
  let {name, img_src, id} = props.people
  return (
    <div className="info-cell" data-id={id}>
      <img className="info-head__img" src={"/img/peoples/" + img_src} alt=""/>
      <span className="info-head__title">{name}</span>
    </div>
  )
}

export default PeopleInTable
