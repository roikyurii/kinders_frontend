import React from 'react'


const AmountBtn = (props) => { 
  let classMod
  props.action === "+" ? classMod = 'increase' : classMod = 'decrease'
  return(
    <button 
      className={"info-cell__btn info-cell__btn--"+classMod}
      data-action={props.action} 
      onClick={props.changeAmount}
      data-id={props.id}>
      {props.action}
    </button>
  )
  
}

export default AmountBtn